import multiprocessing as mp

import numpy
import pandas
from joblib import Parallel, delayed
from sklearn.cluster import KMeans

centroid = 1


def kmeans(a):
    global centroid
    estimators = KMeans(n_clusters=centroid)
    estimators.fit(a)
    return pandas.DataFrame(estimators.cluster_centers_)


def kmeans2(a):
    estimators = KMeans(n_clusters=a['clusters'])
    estimators.fit(a['array'])
    return numpy.around(estimators.cluster_centers_, decimals=6)


def encontrarValor(df, k):
    otimo = -1

    for i in range(2, 100):
        r = numpy.split(df, i)
        if len(r[0]) >= k and i >= k:
            otimo = i
            return otimo
        else:
            pass
    return otimo


def agrupamentoKmeans(df, k):
    pedaco = 1
    tamanho = pedaco * 100
    centros = numpy.ones(shape=(k, df.shape[1]))
    results = pandas.DataFrame(centros)
    if df.shape[0] * df.shape[1] < tamanho:
        centros = kmeans2({'array': df.as_matrix(), 'clusters': k})
    else:
        linhas = int((df.shape[0] / pedaco) * pedaco)
        x = df.iloc[:linhas].as_matrix()
        pool = mp.Pool(2)
        valor = encontrarValor(x, k)
        if valor != -1:
            results = results.append(pool.map(kmeans, numpy.split(x, valor)), ignore_index=True)
            pool.close()
            pool.join()
        else:
            results = results.append(Parallel(n_jobs=2)(delayed(kmeans)(i, k) for i in numpy.split(x, 5)),
                                     ignore_index=True)
        pool.close()
        pool.join()
        r = results[k::]
        a = {'array': r, 'clusters': k}
        centros = kmeans2(a)
    return centros


def aplicarKmeansCoordenadas(conjunto_latitudes, conjunto_longitudes):
    coordenadas_arredondadas = []

    for i in range(0, len(conjunto_latitudes)):
        coordenadas_arredondadas.append([round(conjunto_latitudes[i], 3), round(conjunto_longitudes[i], 3)])
    coordenadas_arredondadas.remove([0.0, 0.0])
    total = []
    contagem = []

    for lat, lon in coordenadas_arredondadas:
        if [lat, lon] not in total:
            total.append([lat, lon])
            contagem.append(coordenadas_arredondadas.count([lat, lon]))

    nova_latitude = []
    nova_longitude = []

    for lat, lon in total:
        nova_latitude.append(lat)
        nova_longitude.append(lon)

    return nova_latitude, nova_longitude, contagem


def aplicarKmeansCoordenadasAtributo(conjunto_latitudes, conjunto_longitudes, conjunto_atributos):
    coordenadas_arredondadas = []

    for i in range(0, len(conjunto_latitudes)):
        coordenadas_arredondadas.append(
            [round(conjunto_latitudes[i], 3), round(conjunto_longitudes[i], 3), conjunto_atributos[i]])
    total = []
    contagem = []
    valor_atributo = 0
    for lat, lon, atr in coordenadas_arredondadas:
        if [lat, lon] not in total:
            total.append([lat, lon])
            for outra_lat, outra_lon, outroAtr in coordenadas_arredondadas:
                if outra_lat == lat and outra_lon == lon:
                    valor_atributo = valor_atributo + outroAtr
            contagem.append(valor_atributo)

    nova_latitude = []
    nova_longitude = []

    for lat, lon in total:
        nova_latitude.append(lat)
        nova_longitude.append(lon)

    return nova_latitude, nova_longitude, contagem
