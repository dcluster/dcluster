import json

import pandas
from django.http import HttpResponse, HttpRequest
from django.views import View
from uuid import uuid4


class Upload(View):
    @staticmethod
    def post(request: HttpRequest) -> str:
        # df: DataFrame = pandas.read_csv(request.FILES['csv'], compression='zip', infer_datetime_format=True, encoding='utf-8')
        # dataset = pandas.read_csv(request.FILES['csv'], infer_datetime_format=True, encoding='utf-8')
        # for col in df.columns:
        # if df[col].dtype == 'object':
        # df[col] = pd.to_datetime(df[col], infer_datetime_format=True)

        token: str = str(uuid4())
        try:
            with open("/tmp/django" + token, 'w') as dataset_file:
                dataset_file.write(str(request.FILES['csv'].read()))

        except Exception as exception:
            response: str = json.dumps({'fail': str(exception)})
            print("Upload::post() Falha ao receber dataset:", exception )
            return HttpResponse(response, content_type="application/json")

        return HttpResponse(json.dumps({'token': token}), content_type="application/json")
