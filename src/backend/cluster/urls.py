# noinspection Mypy
from django.conf.urls import url

import cluster.views.dataset_management

urlpatterns = [
    url(r'^upload/$', cluster.views.dataset_management.Upload.as_view())
]
