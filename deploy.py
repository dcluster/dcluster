#!/usr/bin/env python3
from typing import *
from os import system

NESPED_IP = '200.131.220.164'
USUARIO = 'gabrielcoimbra'

HOST = USUARIO + '@' + NESPED_IP + ':'
RSYNC_EXCLUDES = '--exclue \".git\" --exclude \".idea\" --exclude \".mypy-cache\" --exclude \"__pycache__\" --exclude \"node_modules\" '
RSYNC = 'rsync --verbose --progress --recursive --links ' + RSYNC_EXCLUDES
DIRETORIOS = {'src/backend/':'~/dCluster',
                'src/kepler.gl/':'~/kepler.gl'}

def testarConexao() -> bool:
    if system('ssh -o ConnectTimeout=2 ' + NESPED_IP) != 0:
        return False
    return True

def transferirDiretorios(diretorios: Dict[str, str]) -> None:

    diretorios_separado: List[Tuple(str,str)] = [(x[0], x[1]) for x in diretorios.items()]

    ssh = lambda x: system(RSYNC + x[0] + ' ' + HOST + x[1])
    
    resultados = list(map(ssh , diretorios_separado))
    list(filter(lambda x: print("Erro na transferência") if x != 0 else True , resultados))

def main() -> None:
    if not testarConexao():
        print('ERROR: Não é possível conectar. Lembre-se de ligar a vpn')
        exit(-1)
    transferirDiretorios(DIRETORIOS)

main()
